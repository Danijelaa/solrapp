package rs.laniteo.solrapp.config;

import javax.annotation.Resource;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrOperations;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.data.solr.server.support.EmbeddedSolrServerFactory;
@Configuration
@EnableSolrRepositories(
  basePackages = "rs.laniteo.solrapp.repository",
  namedQueriesLocation = "classpath:application.properties",
  multicoreSupport = true)
@ComponentScan
public class Config {

/*	private final SolrPropertyConfig solrPropertyConfig;

	public ApplicationConfig(SolrPropertyConfig solrPropertyConfig) {
	    this.solrPropertyConfig = solrPropertyConfig;
	}

	@Bean
	public SolrClient solrClient() throws Exception {
	    return new HttpSolrClient.Builder(solrPropertyConfig.getSolrHost()).build();
	}

	@Bean
	public SolrOperations solrTemplate() throws Exception {
	    return new SolrTemplate(solrClient());
	}*/

    @Bean
    public SolrClient solrClient() {
        return new HttpSolrClient("http://localhost:8983/solr");
    }
 
    @Bean
    public SolrTemplate solrTemplate(SolrClient client) throws Exception {
        return new SolrTemplate(client);
    }
}
