package rs.laniteo.solrapp.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.laniteo.solrapp.model.User;
import rs.laniteo.solrapp.repository.UserRepository;

@RestController
@RequestMapping(value="/users")
public class UserController {

	@Autowired
	UserRepository ur;
	
	@RequestMapping(value="/all")
	ResponseEntity<List<User>> getAll(){
		List<User> users=new ArrayList<User>();
		Iterator<User> it=ur.findAll().iterator();
		while(it.hasNext()){
			users.add(it.next());
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	@RequestMapping(value="/{id}")
	ResponseEntity<?> getOne(@PathVariable Integer id){
		User user=(User)ur.findOne(id);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	ResponseEntity<?> delete(@PathVariable Integer id){
		ur.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	ResponseEntity<?> insert(@RequestBody User user){
		ur.save(user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody User user){
		ur.save(user);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
