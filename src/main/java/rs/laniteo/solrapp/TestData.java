package rs.laniteo.solrapp;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rs.laniteo.solrapp.model.User;
import rs.laniteo.solrapp.repository.UserRepository;

@Component
public class TestData {
	@Autowired
	UserRepository ur;
	
	@PostConstruct
	public void init(){
		ur.deleteAll();
		//vccvf
		for(int i=1; i<=5; i++){
			User user=new User();
			user.setId(i);
			user.setName("name"+i);
			ur.save(user);
		}
	}
	//cdff
}
