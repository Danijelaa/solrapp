package rs.laniteo.solrapp.model;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(solrCoreName="users")
public class User {

//jadni user. mOje
//moja izmjena koja stvara konflikt jer ide kasnije
	@Id
	@Field
	//@Indexed(name = "id", type = "integer")
	private int id;
	@Field
	//@Indexed(name = "name", type = "string")
	private String name;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
