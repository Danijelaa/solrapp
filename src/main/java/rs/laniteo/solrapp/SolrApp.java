package rs.laniteo.solrapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolrApp extends org.springframework.boot.web.support.SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(SolrApp.class, args);

	}

}
