package rs.laniteo.solrapp.repository;
import java.io.Serializable;
import java.util.List;






import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import rs.laniteo.solrapp.model.User;
@Component
//@Repository
//@Transactional
public interface UserRepository extends SolrCrudRepository<User, Integer> {

	List<User> findByName(String name);
}
